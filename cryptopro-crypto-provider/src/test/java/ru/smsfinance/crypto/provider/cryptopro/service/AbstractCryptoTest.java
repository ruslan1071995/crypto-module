package ru.smsfinance.crypto.provider.cryptopro.service;

import org.apache.commons.io.FileUtils;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.smsfinance.crypto.provider.cryptopro.config.CryptoProConfiguration;

import java.io.File;
import java.io.IOException;

import static ru.smsfinance.crypto.provider.cryptopro.TestUtils.getInputStream;

/**
 * Abstract class for testing purposes
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public abstract class AbstractCryptoTest {

    @Autowired
    private KeyStoreService keyStoreService;

    @Autowired
    private CryptoProConfiguration cryptoProConfiguration;

    public void saveStores() {
        keyStoreService.unzipIntoStore(getInputStream("intabia.000.zip"));
        keyStoreService.unzipIntoStore(getInputStream("myKey.000.zip"));
    }

    public void cleanStore() {
        try {
            FileUtils.deleteDirectory(new File(cryptoProConfiguration.getKeyStorePath()));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
