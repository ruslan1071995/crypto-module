package ru.smsfinance.crypto.provider.cryptopro.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;
import ru.smsfinance.crypto.provider.cryptopro.tools.SignTools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Sign request service test
 */
public class SignRequestServiceTest extends AbstractCryptoTest {

    private static final String UNSIGNED_MESSAGE = "MESSAGE";

    @Autowired
    private SignService signService;

    @Before
    public void setup() {
        super.saveStores();
    }

    @After
    public void after() {
        super.cleanStore();
    }

    @Test
    public void signTest() {
        byte[] signed = signService.getSigned(getStoreUnlocker(), UNSIGNED_MESSAGE.getBytes());

        assertNotNull("Message not signed", signed);

        String message = SignTools.dropSignature(signed);
        assertEquals("Message with dropped signature not eqauls", UNSIGNED_MESSAGE, message);
    }

    @Test
    public void getSignatureTest() {
        StoreUnlocker storeUnlocker = getStoreUnlocker();

        byte[] signature = signService.getSignature(storeUnlocker, UNSIGNED_MESSAGE.getBytes());

        assertNotNull("Message does not have signature", signature);
    }

    private StoreUnlocker getStoreUnlocker() {
        StoreUnlocker privateKeyUnlocker = new StoreUnlocker();
        privateKeyUnlocker.setAlias("myKey");
        privateKeyUnlocker.setKeyPassword("11111111".toCharArray());
        return privateKeyUnlocker;
    }

}