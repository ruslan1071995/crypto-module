package ru.smsfinance.crypto.provider.cryptopro.service;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.smsfinance.crypto.api.dto.DTOStore;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;
import ru.smsfinance.crypto.core.model.LocalTrustStore;
import ru.smsfinance.crypto.core.model.TrustStoreUploadCast;
import ru.smsfinance.crypto.core.repository.TrustStoreCastRepository;
import ru.smsfinance.crypto.core.repository.TrustStoreRepository;
import ru.smsfinance.crypto.provider.cryptopro.config.service.NbkiConfig;

import java.io.IOException;
import java.security.KeyStore;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static ru.smsfinance.crypto.provider.cryptopro.TestUtils.getInputStream;

/**
 * Trust store service test
 */
public class TrustStoreServiceTest extends AbstractCryptoTest {

    @Autowired
    private TrustStoreService trustStoreService;

    @Autowired
    private TrustStoreRepository trustStoreRepository;

    @Autowired
    private TrustStoreCastRepository trustStoreCastRepository;

    @Autowired
    private NbkiConfig nbkiConfig;

    @Before
    public void setup() {
        super.saveStores();
    }

    @After
    public void after() {
        super.cleanStore();
    }

    /**
     * Saving trust store test
     */
    @Test
    public void saveTrustStore() throws IOException {
        DTOStore dtoTrustStore = new DTOStore();
        dtoTrustStore.setFile(IOUtils.toByteArray(getInputStream("certstore")));
        dtoTrustStore.setAlias("intabia");
        dtoTrustStore.setStandard("AbraCadabra");
        trustStoreService.saveTrustStoreAndUpdateServiceConfigs(nbkiConfig, dtoTrustStore);

        LocalTrustStore trustStore = trustStoreRepository.findFirstByKeyIdOrderByUploadTimeDesc("intabia");
        TrustStoreUploadCast trustStoreCast = trustStoreCastRepository
                .findByTrustStoreOrderByUploadTimeDesc(trustStore);

        assertEquals(dtoTrustStore.getAlias(), trustStore.getKeyId());
        assertEquals(dtoTrustStore.getStandard(), trustStore.getStandard());
        assertEquals(new String(dtoTrustStore.getFile()),
                new String(trustStoreCast.getFile()));
        assertEquals(trustStoreCast.getTrustStore(), trustStore);
    }

    @Test
    public void getTrustStoreTest() throws IOException {
        String alias = "myalias";
        char[] password = "0000".toCharArray();

        StoreUnlocker storeUnlocker = new StoreUnlocker();
        storeUnlocker.setAlias(alias);
        storeUnlocker.setStorePassword(password);

        DTOStore dtoTrustStore = new DTOStore();
        dtoTrustStore.setAlias(alias);
        dtoTrustStore.setFile(IOUtils.toByteArray(getInputStream("certstore")));

        trustStoreService.saveTrustStoreAndUpdateServiceConfigs(nbkiConfig, dtoTrustStore);
        KeyStore trustStore = trustStoreService.getTrustStore(nbkiConfig.getTrustStoreUnlocker());

        assertNotNull("Trust store must not be null", trustStore);
    }

    @Test
    public void updateSettingsTest() throws IOException {
        String newAlias = UUID.randomUUID().toString();
        String storePassword = UUID.randomUUID().toString();

        DTOStore dtoTrustStore = new DTOStore();
        dtoTrustStore.setAlias(newAlias);
        dtoTrustStore.setFile(IOUtils.toByteArray(getInputStream("certstore")));
        dtoTrustStore.setStorePassword(storePassword);

        trustStoreService.saveTrustStoreAndUpdateServiceConfigs(nbkiConfig, dtoTrustStore);

        assertEquals(newAlias, nbkiConfig.getTrustStoreUnlocker().getAlias());
        assertEquals(storePassword, new String(nbkiConfig.getTrustStoreUnlocker().getStorePassword()));
    }
}