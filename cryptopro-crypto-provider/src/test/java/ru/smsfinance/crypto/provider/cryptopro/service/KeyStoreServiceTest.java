package ru.smsfinance.crypto.provider.cryptopro.service;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.smsfinance.crypto.api.dto.DTOStore;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;
import ru.smsfinance.crypto.provider.cryptopro.config.service.NbkiConfig;
import ru.smsfinance.crypto.provider.cryptopro.tools.KeyStoreTools;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.UUID;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static ru.smsfinance.crypto.provider.cryptopro.TestUtils.getInputStream;

public class KeyStoreServiceTest extends AbstractCryptoTest {

    @Autowired
    private KeyStoreService keyStoreService;

    @Autowired
    private NbkiConfig nbkiConfig;

    @Before
    public void setup() {
        super.cleanStore();
    }

    @After
    public void after() {
        super.cleanStore();
    }

    /**
     * Save key store test
     */
    @Test
    public void saveKeyStore() throws Exception {
        byte[] keyStoreBytes = IOUtils.toByteArray(getClass().getClassLoader()
                .getResourceAsStream("myKey.000.zip"));

        DTOStore dtoKeyStore = new DTOStore();
        dtoKeyStore.setAlias("myKey");
        dtoKeyStore.setFile(keyStoreBytes);

        StoreUnlocker privateKeyUnlocker = new StoreUnlocker();
        privateKeyUnlocker.setKeyPassword("11111111".toCharArray());
        privateKeyUnlocker.setAlias("myKey");

        keyStoreService.saveKeyStoreAndUpdateServiceConfigs(nbkiConfig, dtoKeyStore);

        PrivateKey privateKey = KeyStoreTools.loadKey(privateKeyUnlocker);
        assertNotNull("Unzipped and loaded private key can not be null", privateKey);
    }


    @Test
    public void updateServiceSettingsTest() throws IOException {
        String newAlias = UUID.randomUUID().toString();
        String newKeyPassword = UUID.randomUUID().toString();
        String newStorePassword = UUID.randomUUID().toString();

        DTOStore dtoStore = new DTOStore();
        dtoStore.setAlias(newAlias);
        dtoStore.setStorePassword(newStorePassword);
        dtoStore.setKeyPassword(newKeyPassword);
        dtoStore.setFile(IOUtils.toByteArray(getInputStream("myKey.000.zip")));

        keyStoreService.saveKeyStoreAndUpdateServiceConfigs(nbkiConfig, dtoStore);

        assertEquals(newAlias, nbkiConfig.getKeyStoreUnlocker().getAlias());
        assertArrayEquals(newKeyPassword.toCharArray(), nbkiConfig.getKeyStoreUnlocker().getKeyPassword());
        assertArrayEquals(newStorePassword.toCharArray(), nbkiConfig.getKeyStoreUnlocker().getStorePassword());
    }
}