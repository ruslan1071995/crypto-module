package ru.smsfinance.crypto.provider.cryptopro.service.it;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.smsfinance.crypto.api.dto.DTOStore;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;
import ru.smsfinance.crypto.core.keystore.StoreUnlockerBuilder;
import ru.smsfinance.crypto.provider.cryptopro.service.AbstractCryptoTest;
import ru.smsfinance.crypto.provider.cryptopro.service.SSLService;
import ru.smsfinance.crypto.provider.cryptopro.service.TrustStoreService;

import javax.net.ssl.SSLContext;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static ru.smsfinance.crypto.provider.cryptopro.TestUtils.getInputStream;

/**
 * CryptoPro SSL request test
 */
public class CryptoProSSLTest extends AbstractCryptoTest {

    @Autowired
    private SSLService sslService;

    @Autowired
    private TrustStoreService trustStoreService;

    private static final String GOST_SSL_WITH_AUTH = "https://tlsgost-2001auth.cryptopro.ru/index.html";
    private static final String GOST_SSL_NO_AUTH = "https://tlsgost-2001.cryptopro.ru/index.html";

    @Before
    public void before() {
        super.saveStores();
    }

    @After
    public void after() {
        super.cleanStore();
    }

    /**
     * Test SSL request with client authentication
     */
    @Test
    public void sslRequestWithAuthTest() throws IOException {
        CloseableHttpResponse response = doSSLRequest(GOST_SSL_WITH_AUTH);
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    /**
     * Test SSL request without client authentication
     */
    @Test
    public void sslRequestNoAuthTest() throws IOException {
        CloseableHttpResponse response = doSSLRequest(GOST_SSL_NO_AUTH);
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private CloseableHttpResponse doSSLRequest(String url) throws IOException {
        StoreUnlocker keyStoreUnlocker = new StoreUnlocker();
        keyStoreUnlocker.setAlias("intabia");
        keyStoreUnlocker.setKeyPassword("11111111".toCharArray());

        DTOStore dtoTrustStore = new DTOStore();
        dtoTrustStore.setFile(IOUtils.toByteArray(getInputStream("certstore")));
        dtoTrustStore.setAlias("intabia");
        dtoTrustStore.setStandard("CertStore");
        dtoTrustStore.setStorePassword("0000");

        StoreUnlocker trustStoreUnlocker = StoreUnlockerBuilder.buildFrom(dtoTrustStore);

        trustStoreService.saveTrustStore(dtoTrustStore);

        SSLContext initializedSSLContext = sslService.getInitializedSSLContext(keyStoreUnlocker, trustStoreUnlocker);

        CloseableHttpClient httpClient = HttpClients.custom().setSSLContext(initializedSSLContext).build();
        HttpGet getMethod = new HttpGet(GOST_SSL_WITH_AUTH);
        return httpClient.execute(getMethod);
    }
}
