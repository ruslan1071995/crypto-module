package ru.smsfinance.crypto.provider.cryptopro;

import java.io.InputStream;

public final class TestUtils {

    private TestUtils() {
    }

    public static InputStream getInputStream(String fileName) {
        return TestUtils.class.getClassLoader().getResourceAsStream(fileName);
    }
}
