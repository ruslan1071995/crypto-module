package ru.smsfinance.crypto.provider.cryptopro.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.CryptoPro.JCP.JCP;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;

import java.io.ByteArrayInputStream;
import java.security.DigestInputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.cert.Certificate;

/**
 * Key store tools utils class
 */
public final class KeyStoreTools {

    private KeyStoreTools() {
    }

    /**
     * logger
     */
    public static final Logger logger = LoggerFactory.getLogger("LOG");

    /**
     * алгоритмы и т.д.
     */
    public static final String STORE_TYPE = "HDImageStore";
    public static final String DIGEST_ALG_NAME = JCP.GOST_DIGEST_NAME;

    /**
     * OIDs для CMS
     */
    public static final String STR_CMS_OID_DATA = "1.2.840.113549.1.7.1";
    public static final String STR_CMS_OID_SIGNED = "1.2.840.113549.1.7.2";
    public static final String STR_CMS_OID_ENVELOPED = "1.2.840.113549.1.7.3";

    public static final String STR_CMS_OID_CONT_TYP_ATTR = "1.2.840.113549.1.9.3";
    public static final String STR_CMS_OID_DIGEST_ATTR = "1.2.840.113549.1.9.4";
    public static final String STR_CMS_OID_SIGN_TYM_ATTR = "1.2.840.113549.1.9.5";

    public static final String STR_CMS_OID_TS = "1.2.840.113549.1.9.16.1.4";

    public static final String DIGEST_OID = JCP.GOST_DIGEST_OID;
    public static final String SIGN_OID = JCP.GOST_EL_KEY_OID;


    /**
     * Loading private key
     *
     * @param unlocker private key unlocker
     * @return constructed private key
     * @throws Exception in private key read
     */
    public static PrivateKey loadKey(StoreUnlocker unlocker)
            throws Exception {
        return (PrivateKey) loadAndGetKeyStore().getKey(unlocker.getAlias(), unlocker.getKeyPassword());
    }

    /**
     * Loading private key from key store
     *
     * @param keyStore key store
     * @param unlocker private key unlocker
     * @return constructed private key
     * @throws Exception in private key read
     */
    public static PrivateKey loadKey(KeyStore keyStore, StoreUnlocker unlocker)
            throws Exception {
        return (PrivateKey) keyStore.getKey(unlocker.getAlias(), unlocker.getKeyPassword());
    }

    /**
     * Load key store and return it
     *
     * @return loaded key store
     * @throws Exception in loading keystore
     */
    public static KeyStore loadAndGetKeyStore() throws Exception {
        final KeyStore keyStore = KeyStore.getInstance(STORE_TYPE);
        keyStore.load(null, null);
        return keyStore;
    }

    /**
     * Load in-memory key store and return it
     *
     * @return loaded key store
     * @throws Exception in loading keystore
     */
    public static KeyStore loadAndGetInMemoryKeyStore() throws Exception {
        final KeyStore keyStore = KeyStore.getInstance(JCP.MEMORY_STORE_NAME);
        keyStore.load(null, null);
        return keyStore;
    }


    /**
     * Create in-memory(RAM) key store from another key store
     *
     * @param keyStore         original key store
     * @param originalUnlocker original key store unlocker
     * @return copied key store
     * @throws Exception in loading key store or copying it to memory
     */
    public static KeyStore copyKeyStoreToMemory(KeyStore keyStore,
                                                StoreUnlocker originalUnlocker) throws Exception {
        KeyStore tmpKeyStore = KeyStoreTools.loadAndGetInMemoryKeyStore();
        // Устанавливаем в key store в памяти приватный ключ и сертификат из keystore
        tmpKeyStore.setKeyEntry(originalUnlocker.getAlias(), KeyStoreTools.loadKey(keyStore, originalUnlocker),
                originalUnlocker.getKeyPassword(),
                new Certificate[]{keyStore.getCertificate(originalUnlocker.getAlias())});
        // Сохраняем
        tmpKeyStore.store(null, null);
        return tmpKeyStore;

    }

    /**
     * Получение certificate из store.
     *
     * @param privateKeyUnlocker private key unlocker
     * @return Certificate
     * @throws Exception in cert read
     */
    public static Certificate loadCertificate(StoreUnlocker privateKeyUnlocker)
            throws Exception {
        return loadAndGetKeyStore().getCertificate(privateKeyUnlocker.getAlias());
    }

    /**
     * Получение certificate из store.
     *
     * @param keyStore           key store
     * @param privateKeyUnlocker private key unlocker
     * @return Certificate
     * @throws Exception in cert read
     */
    public static Certificate loadCertificate(KeyStore keyStore, StoreUnlocker privateKeyUnlocker)
            throws Exception {
        return keyStore.getCertificate(privateKeyUnlocker.getAlias());
    }

    /**
     * @param bytes               bytes
     * @param digestAlgorithmName algorithm
     * @return digest
     * @throws Exception e
     */
    public static byte[] digestm(byte[] bytes, String digestAlgorithmName) throws Exception {
        //calculation messageDigest
        final MessageDigest digest = MessageDigest.getInstance(digestAlgorithmName);
        try (DigestInputStream digestStream = new DigestInputStream(new ByteArrayInputStream(bytes), digest)) {
            while (digestStream.available() != 0) {
                digestStream.read();
            }
            return digest.digest();
        }
    }
}
