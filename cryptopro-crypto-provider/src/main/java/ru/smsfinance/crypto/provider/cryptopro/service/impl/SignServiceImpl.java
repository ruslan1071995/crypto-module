package ru.smsfinance.crypto.provider.cryptopro.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;
import ru.smsfinance.crypto.provider.cryptopro.exception.CryptoProException;
import ru.smsfinance.crypto.provider.cryptopro.model.enums.SignatureType;
import ru.smsfinance.crypto.provider.cryptopro.service.SignService;
import ru.smsfinance.crypto.provider.cryptopro.tools.KeyStoreTools;
import ru.smsfinance.crypto.provider.cryptopro.tools.SignTools;

import java.security.PrivateKey;
import java.security.cert.Certificate;

/**
 * Sign service implementation
 */
@Service
@Slf4j
public class SignServiceImpl implements SignService {

    @Override
    public byte[] getSigned(StoreUnlocker storeUnlocker, byte[] body) {
        log.debug("Start got SIGNED WITH FILE for unlocker: {}", storeUnlocker);
        return getBytes(storeUnlocker, body, false);
    }

    @Override
    public byte[] getSignature(StoreUnlocker storeUnlocker, byte[] file) {
        log.debug("Start got SIGNATURE request for unlocker: {}", storeUnlocker);
        return getBytes(storeUnlocker, file, true);
    }

    private byte[] getBytes(StoreUnlocker storeUnlocker, byte[] body, boolean onlySignature) {
        try {
            PrivateKey privateKey = KeyStoreTools.loadKey(storeUnlocker);
            Certificate certificate = KeyStoreTools.loadCertificate(storeUnlocker);

            return SignTools.getSigned(body,
                    SignatureType.LOW_SIGN, privateKey, certificate, onlySignature);
        } catch (Exception e) {
            log.error("Error while get signed file", e);
            throw new CryptoProException("Internal crypto exception");
        }
    }
}
