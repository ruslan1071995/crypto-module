package ru.smsfinance.crypto.provider.cryptopro.config.service;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import ru.smsfinance.crypto.core.keystore.ServiceConfiguration;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;

/**
 * Equifax service config
 */
@Data
@ConfigurationProperties("services.equifax")
public class EquifaxConfig implements ServiceConfiguration {
    /**
     * Store unlocker for equifax key store
     */
    private StoreUnlocker keyStoreUnlocker;

    private StoreUnlocker trustStoreUnlocker;

    /**
     * Equifax zuul filter order
     */
    private Integer filterOrder;
}
