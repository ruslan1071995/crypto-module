package ru.smsfinance.crypto.provider.cryptopro.service;

import ru.smsfinance.crypto.api.dto.DTOStore;
import ru.smsfinance.crypto.core.keystore.ServiceConfiguration;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;

import java.io.InputStream;
import java.security.KeyStore;

/**
 * Service for managing key stores
 */
public interface KeyStoreService {

    /**
     * Save key store and update service configs
     *
     * @param serviceConfiguration service configuration
     * @param dtoKeyStore          request dto
     */
    void saveKeyStoreAndUpdateServiceConfigs(ServiceConfiguration serviceConfiguration, DTOStore dtoKeyStore);

    /**
     * Create in-memory(RAM) key store from another key store
     *
     * @param keyStore original key store
     * @param unlocker original key store unlocker
     * @return copied key store
     */
    KeyStore copyKeyStoreToMemory(KeyStore keyStore, StoreUnlocker unlocker);

    /**
     * Unzip on HDD store
     *
     * @param zipInputStream zip file input stream
     */
    void unzipIntoStore(InputStream zipInputStream);
}
