package ru.smsfinance.crypto.provider.cryptopro.config;

import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;
import ru.smsfinance.crypto.api.dto.Service;
import ru.smsfinance.crypto.core.config.CoreConfig;
import ru.smsfinance.crypto.core.keystore.ServiceConfiguration;
import ru.smsfinance.crypto.provider.cryptopro.config.service.EquifaxConfig;
import ru.smsfinance.crypto.provider.cryptopro.config.service.NbkiConfig;

import java.util.EnumMap;
import java.util.Map;

/**
 * Crypto provider configuration
 */
@Configuration
@EnableZuulProxy
@Import(CoreConfig.class)
public class CryptoProProviderApplicationConfiguration {

    /**
     * Rest template bean
     *
     * @return rest template bean
     */
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * CryptoPro configuration bean
     *
     * @return cryptoPro properties
     */
    @Bean
    public CryptoProConfiguration cryptoProConfiguration() {
        return new CryptoProConfiguration();
    }

    /**
     * Initialize system properties
     *
     * @return system initializer bean
     */
    @Bean
    public SystemInitializer systemInitializer() {
        return new SystemInitializer(cryptoProConfiguration());
    }

    /**
     * Create nbki service configurations bean
     *
     * @return nbki service configurations bean
     */
    @Bean
    public NbkiConfig nbkiConfig() {
        return new NbkiConfig();
    }

    /**
     * Create equifax service configuration bean
     *
     * @return equifax service configuration bean
     */
    @Bean
    public EquifaxConfig equifaxConfig() {
        return new EquifaxConfig();
    }

    /**
     * Init map of service enum to service configuration
     *
     * @return created map bean
     */
    @Bean
    public Map<Service, ServiceConfiguration> serviceConfigurationMap() {
        Map<Service, ServiceConfiguration> serviceConfigurationEnumMap = new EnumMap<>(Service.class);
        serviceConfigurationEnumMap.put(Service.EQUIFAX, equifaxConfig());
        serviceConfigurationEnumMap.put(Service.NBKI, nbkiConfig());

        return serviceConfigurationEnumMap;
    }
}
