package ru.smsfinance.crypto.provider.cryptopro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * CryptoPro provider application
 */
@SpringBootApplication
public class CryptoProProviderApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CryptoProProviderApplication.class);
    }

    /**
     * Application start point
     *
     * @param args args
     */
    public static void main(String[] args) {
        SpringApplication.run(CryptoProProviderApplication.class, args);
    }
}
