package ru.smsfinance.crypto.provider.cryptopro.config.service;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationEventPublisher;
import ru.smsfinance.crypto.core.keystore.ServiceConfiguration;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;
import ru.smsfinance.crypto.provider.cryptopro.event.UpdateNbkiSettingsEvent;

/**
 * Service configuration
 */
@ConfigurationProperties(prefix = "services.nbki")
@Data
@NoArgsConstructor
public class NbkiConfig implements ServiceConfiguration {

    @Autowired
    private ApplicationEventPublisher publisher;

    /**
     * Store unlocker for key store
     */
    private StoreUnlocker keyStoreUnlocker;

    /**
     * Store unlocker for trust store
     */
    private StoreUnlocker trustStoreUnlocker;

    /**
     * Zuul filter order number.
     */
    private Integer filterOrder;

    /**
     * Setter with udpate settings publish event
     *
     * @param keyStoreUnlocker key store unlocker
     */
    public void setKeyStoreUnlocker(StoreUnlocker keyStoreUnlocker) {
        this.keyStoreUnlocker = keyStoreUnlocker;
        publisher.publishEvent(new UpdateNbkiSettingsEvent(this));
    }

    /**
     * Setter with udpate settings publish event
     *
     * @param trustStoreUnlocker trust store unlocker
     */
    public void setTrustStoreUnlocker(StoreUnlocker trustStoreUnlocker) {
        this.trustStoreUnlocker = trustStoreUnlocker;
        publisher.publishEvent(new UpdateNbkiSettingsEvent(this));
    }
}
