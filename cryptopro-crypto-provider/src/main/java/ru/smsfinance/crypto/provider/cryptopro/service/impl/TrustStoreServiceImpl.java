package ru.smsfinance.crypto.provider.cryptopro.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.CryptoPro.JCP.JCP;
import ru.smsfinance.crypto.api.dto.DTOStore;
import ru.smsfinance.crypto.core.keystore.ServiceConfiguration;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;
import ru.smsfinance.crypto.core.keystore.StoreUnlockerBuilder;
import ru.smsfinance.crypto.core.model.LocalTrustStore;
import ru.smsfinance.crypto.core.model.TrustStoreUploadCast;
import ru.smsfinance.crypto.core.repository.TrustStoreCastRepository;
import ru.smsfinance.crypto.core.repository.TrustStoreRepository;
import ru.smsfinance.crypto.provider.cryptopro.config.CryptoProConfiguration;
import ru.smsfinance.crypto.provider.cryptopro.exception.CryptoProException;
import ru.smsfinance.crypto.provider.cryptopro.service.TrustStoreService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.util.Optional;
import java.util.UUID;

/**
 * Trust store service implementation
 */
@Service
@Slf4j
public class TrustStoreServiceImpl implements TrustStoreService {

    private TrustStoreRepository trustStoreRepository;

    private TrustStoreCastRepository trustStoreCastRepository;

    private CryptoProConfiguration cryptoProConfiguration;

    /**
     * Autowiring constructor
     *
     * @param trustStoreRepository     trust store repository
     * @param trustStoreCastRepository trust store cast repository
     * @param cryptoProConfiguration   cryptoPro configuration bean
     */
    @Autowired
    public TrustStoreServiceImpl(TrustStoreRepository trustStoreRepository,
                                 TrustStoreCastRepository trustStoreCastRepository,
                                 CryptoProConfiguration cryptoProConfiguration) {
        this.trustStoreRepository = trustStoreRepository;
        this.trustStoreCastRepository = trustStoreCastRepository;
        this.cryptoProConfiguration = cryptoProConfiguration;
    }

    @Override
    public void saveTrustStoreAndUpdateServiceConfigs(ServiceConfiguration trustStoreInfo, DTOStore dtoTrustStore) {
        saveTrustStore(dtoTrustStore);
        trustStoreInfo.setTrustStoreUnlocker(StoreUnlockerBuilder.buildFrom(dtoTrustStore));
    }

    @Override
    public void saveTrustStore(DTOStore dtoTrustStore) {
        log.info("START saving trust store with key id: {}", dtoTrustStore.getAlias());
        TrustStoreUploadCast uploadCast = preSaveAndGet(dtoTrustStore);
        String path = saveToFileSystem(dtoTrustStore);

        LocalTrustStore trustStore = new LocalTrustStore();
        trustStore.setKeyId(dtoTrustStore.getAlias());
        trustStore.setPath(path);
        trustStore.setStandard(dtoTrustStore.getStandard() == null ?
                JCP.CERT_STORE_NAME : dtoTrustStore.getStandard());

        trustStore = trustStoreRepository.saveAndFlush(trustStore);
        log.info("Saved trust store: {}", trustStore);

        uploadCast.setTrustStore(trustStore);
        trustStoreCastRepository.saveAndFlush(uploadCast);
        log.info("FINISH saving trust store with key id: {}", dtoTrustStore.getAlias());
    }

    @Override
    public KeyStore getTrustStore(StoreUnlocker storeUnlocker) {
        LocalTrustStore trustStoreEntity = trustStoreRepository
                .findFirstByKeyIdOrderByUploadTimeDesc(storeUnlocker.getAlias());

        if (trustStoreEntity == null) {
            log.info("Not found trust store for: {}", storeUnlocker);
            return null;
        }

        try (InputStream fis = new FileInputStream(new File(trustStoreEntity.getPath()))) {
            KeyStore trustStore = KeyStore.getInstance(trustStoreEntity.getStandard());

            char[] password = Optional.ofNullable(storeUnlocker.getStorePassword())
                    .orElse(null);

            trustStore.load(fis, password);
            return trustStore;
        } catch (Exception e) {
            log.error("Error while loading trust store from file system", e);
            throw new CryptoProException("Error while loading trust store from file system", e);
        }

    }

    private String saveToFileSystem(DTOStore dtoTrustStore) {
        try {
            String path = Paths.get(cryptoProConfiguration.getTrustStorePath(),
                    generateTrustStorePath(dtoTrustStore.getAlias())).toString();

            log.debug("Saving trust store to path: {}", path);
            FileUtils.writeByteArrayToFile(new File(path), dtoTrustStore.getFile());
            return path;
        } catch (IOException e) {
            log.error("Error while saving trust store", e);
            throw new CryptoProException("Error while saving trust store", e);
        }
    }

    private TrustStoreUploadCast preSaveAndGet(DTOStore dtoTrustStore) {
        TrustStoreUploadCast uploadCast = new TrustStoreUploadCast();
        uploadCast.setFile(dtoTrustStore.getFile());

        uploadCast = trustStoreCastRepository.saveAndFlush(uploadCast);
        log.debug("Saved trust store cast: {}", uploadCast);
        return uploadCast;
    }

    private String generateTrustStorePath(String keyId) {
        return Paths.get(keyId, UUID.randomUUID().toString()).toString();
    }
}
