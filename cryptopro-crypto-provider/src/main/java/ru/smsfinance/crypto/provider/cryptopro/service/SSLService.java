package ru.smsfinance.crypto.provider.cryptopro.service;

import ru.smsfinance.crypto.core.keystore.StoreUnlocker;

import javax.net.ssl.SSLContext;

/**
 * SSL service
 */
public interface SSLService {

    /**
     * Get initialized ssl context
     *
     * @param keyStoreUnlocker   key store unlocker
     * @param trustStoreUnlocker trust store unlocker
     * @return constructed ssl context
     */
    SSLContext getInitializedSSLContext(StoreUnlocker keyStoreUnlocker, StoreUnlocker trustStoreUnlocker);

    /**
     * Get ssl context instance. GostTLS protocol
     * This instance not initialized
     *
     * @return ssl context
     */
    SSLContext getSSLContextInstance();

    /**
     * Initialize ssl context by key store and trust store
     *
     * @param sslContext ssl context to initialize
     * @param keyStoreUnlocker   key store unlocker
     * @param trustStoreUnlocker trust store unlocker
     */
    void initializeSSLContext(SSLContext sslContext, StoreUnlocker keyStoreUnlocker, StoreUnlocker trustStoreUnlocker);
}
