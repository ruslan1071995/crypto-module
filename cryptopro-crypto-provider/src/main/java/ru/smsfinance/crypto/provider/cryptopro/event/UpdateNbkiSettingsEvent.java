package ru.smsfinance.crypto.provider.cryptopro.event;

import org.springframework.context.ApplicationEvent;
import ru.smsfinance.crypto.provider.cryptopro.config.service.NbkiConfig;

/**
 * Event published when some NBKI settings has been chaged
 */
public class UpdateNbkiSettingsEvent extends ApplicationEvent {

    private NbkiConfig nbkiConfig;

    /**
     * Create a new Update Nbki Settings Event.
     *
     * @param nbkiConfig the object on which the event initially occurred
     */
    public UpdateNbkiSettingsEvent(NbkiConfig nbkiConfig) {
        super(nbkiConfig);
        this.nbkiConfig = nbkiConfig;
    }

    public NbkiConfig getNbkiConfig() {
        return nbkiConfig;
    }
}
