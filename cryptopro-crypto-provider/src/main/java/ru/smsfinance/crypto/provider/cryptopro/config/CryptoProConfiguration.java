package ru.smsfinance.crypto.provider.cryptopro.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * CryptoPro properties
 */
@ConfigurationProperties(prefix = "cryptoPro")
@Data
public class CryptoProConfiguration {

    /**
     * Key store path
     */
    private String keyStorePath;

    /**
     * Trust store path
     */
    private String trustStorePath;

    /**
     * System properties for {@link SystemInitializer}
     */
    private Map<String, String> system;
}
