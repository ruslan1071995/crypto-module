package ru.smsfinance.crypto.provider.cryptopro.config;

import lombok.extern.slf4j.Slf4j;
import ru.CryptoPro.JCP.KeyStore.HDImage.HDImageStore;

import java.util.Map;
import java.util.prefs.Preferences;

/**
 * System initializer. Need to set system properties
 */
@Slf4j
public class SystemInitializer {

    /**
     * Constructor that initialize system properties and set default HDImageStore directory
     *
     * @param cryptoProConfiguration config properties
     */
    public SystemInitializer(CryptoProConfiguration cryptoProConfiguration) {
        log.warn("Set HDIMAGESTORE directory: {}", cryptoProConfiguration.getKeyStorePath());
        HDImageStore.setDir(cryptoProConfiguration.getKeyStorePath());

        log.warn("Disable default revocation for SSL");
        Preferences.userRoot()
                .node("ru/CryptoPro/ssl")
                .put("Enable_revocation_default", "false");

        Map<String, String> systemProperties = cryptoProConfiguration.getSystem();
        if (systemProperties == null) {
            return;
        }
        systemProperties.forEach((key, value) -> {
            log.warn("System set property [{}: {}]", key, value);
            System.setProperty(key, value);
        });
    }
}
