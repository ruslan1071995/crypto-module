package ru.smsfinance.crypto.provider.cryptopro.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.zeroturnaround.zip.ZipUtil;
import ru.smsfinance.crypto.api.dto.DTOStore;
import ru.smsfinance.crypto.core.keystore.ServiceConfiguration;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;
import ru.smsfinance.crypto.core.keystore.StoreUnlockerBuilder;
import ru.smsfinance.crypto.core.model.KeyStoreUploadCast;
import ru.smsfinance.crypto.core.model.LocalKeyStore;
import ru.smsfinance.crypto.core.repository.KeyStoreCastRepository;
import ru.smsfinance.crypto.core.repository.KeyStoreRepository;
import ru.smsfinance.crypto.provider.cryptopro.config.CryptoProConfiguration;
import ru.smsfinance.crypto.provider.cryptopro.exception.CryptoProException;
import ru.smsfinance.crypto.provider.cryptopro.service.KeyStoreService;
import ru.smsfinance.crypto.provider.cryptopro.tools.KeyStoreTools;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.security.KeyStore;

/**
 * Key store service implementation
 */
@Service
@Slf4j
public class KeyStoreServiceImpl implements KeyStoreService {

    private CryptoProConfiguration cryptoProConfiguration;

    private KeyStoreCastRepository keyStoreCastRepository;

    private KeyStoreRepository keyStoreRepository;

    /**
     * Autowiring constructor
     *
     * @param cryptoProConfiguration cryptoPro configuration bean
     * @param keyStoreCastRepository key store cast repository for backup purposes
     * @param keyStoreRepository     key store repository
     */
    public KeyStoreServiceImpl(CryptoProConfiguration cryptoProConfiguration,
                               KeyStoreCastRepository keyStoreCastRepository,
                               KeyStoreRepository keyStoreRepository) {
        this.cryptoProConfiguration = cryptoProConfiguration;
        this.keyStoreCastRepository = keyStoreCastRepository;
        this.keyStoreRepository = keyStoreRepository;
    }

    @Override
    public void saveKeyStoreAndUpdateServiceConfigs(ServiceConfiguration keyStoreInfo, DTOStore dtoKeyStore) {
        KeyStoreUploadCast keyStoreCast = preSaveUploadCast(dtoKeyStore);
        unzipIntoStore(new ByteArrayInputStream(dtoKeyStore.getFile()));
        LocalKeyStore localKeyStore = saveLocalKeyStore(dtoKeyStore);

        keyStoreCast.setKeyStore(localKeyStore);
        keyStoreCastRepository.saveAndFlush(keyStoreCast);

        keyStoreInfo.setKeyStoreUnlocker(StoreUnlockerBuilder.buildFrom(dtoKeyStore));

        log.info("Saved key store for alias: {}", localKeyStore.getKeyId());
    }

    @Override
    public KeyStore copyKeyStoreToMemory(KeyStore keyStore, StoreUnlocker originalUnlocker) {
        try {
            log.info("Start copying key store to MEMORY for keyId: {}", originalUnlocker.getAlias());
            return KeyStoreTools.copyKeyStoreToMemory(keyStore, originalUnlocker);
        } catch (Exception e) {
            log.error("Error while copying key store to memory", e);
            throw new CryptoProException("Error while copying key store to memory", e);
        }
    }

    @Override
    public void unzipIntoStore(InputStream zipInputStream) {
        log.trace("START unzipping key store to local file system");
        ZipUtil.unpack(zipInputStream, new File(cryptoProConfiguration.getKeyStorePath()),
                name -> {
                    log.trace("Unzipped file: {}", name);
                    return name.matches("^.*\\.000.*$") ? name : null;
                });
        log.trace("FINISH unzipping key store to local file system");
    }

    private LocalKeyStore saveLocalKeyStore(DTOStore dtoKeyStore) {
        LocalKeyStore localKeyStore = new LocalKeyStore();
        localKeyStore.setKeyId(dtoKeyStore.getAlias());

        localKeyStore = keyStoreRepository.saveAndFlush(localKeyStore);
        log.info("Finish saving key store: {}", localKeyStore);
        return localKeyStore;
    }

    private KeyStoreUploadCast preSaveUploadCast(DTOStore dtoKeyStore) {
        log.debug("Pre saving key store for alias: {}", dtoKeyStore.getAlias());
        KeyStoreUploadCast keyStoreCast = new KeyStoreUploadCast();
        keyStoreCast.setFile(dtoKeyStore.getFile());
        keyStoreCast = keyStoreCastRepository.saveAndFlush(keyStoreCast);
        log.debug("Finish saving key store upload cast: {}", keyStoreCast);
        return keyStoreCast;
    }
}
