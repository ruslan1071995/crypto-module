package ru.smsfinance.crypto.provider.cryptopro.service;

import ru.smsfinance.crypto.core.keystore.StoreUnlocker;

/**
 * Service for signing files
 */
public interface SignService {

    /**
     * Sign body
     *
     * @param storeUnlocker key store unlocker
     * @param file          file that we need to sign
     * @return signed message body in bytes
     */
    byte[] getSigned(StoreUnlocker storeUnlocker, byte[] file);

    /**
     * Get signature separately from file
     *
     * @param storeUnlocker key store unlocker
     * @param file          file that we need to get signature
     * @return file signature
     */
    byte[] getSignature(StoreUnlocker storeUnlocker, byte[] file);
}
