package ru.smsfinance.crypto.provider.cryptopro.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.smsfinance.crypto.api.controller.ServiceApi;
import ru.smsfinance.crypto.api.dto.DTOStore;
import ru.smsfinance.crypto.api.dto.Service;
import ru.smsfinance.crypto.core.keystore.ServiceConfiguration;
import ru.smsfinance.crypto.provider.cryptopro.service.KeyStoreService;
import ru.smsfinance.crypto.provider.cryptopro.service.SignService;
import ru.smsfinance.crypto.provider.cryptopro.service.TrustStoreService;

import java.io.IOException;
import java.util.Map;

/**
 * Nbki controller
 */
@RestController
@Slf4j
public class CryptoProProviderController implements ServiceApi {

    private KeyStoreService keyStoreService;
    private TrustStoreService trustStoreService;
    private SignService signService;
    private Map<Service, ServiceConfiguration> serviceConfigurationMap;

    /**
     * Autowiring constructor
     *
     * @param keyStoreService         key store service
     * @param trustStoreService       trust store service
     * @param signService             sign service
     * @param serviceConfigurationMap service configuration map for got settings for current service
     */
    @Autowired
    public CryptoProProviderController(KeyStoreService keyStoreService, TrustStoreService trustStoreService,
                                       SignService signService,
                                       Map<Service, ServiceConfiguration> serviceConfigurationMap) {
        this.keyStoreService = keyStoreService;
        this.trustStoreService = trustStoreService;
        this.signService = signService;
        this.serviceConfigurationMap = serviceConfigurationMap;
    }

    @Override
    public ResponseEntity<Resource> getSignature(String serviceId, MultipartFile file) {
        byte[] signature = signService.getSignature(getServiceConfiguration(serviceId).getKeyStoreUnlocker(),
                getBytesFromRequestFile(file));

        return getOkByteResponse(signature);
    }

    @Override
    public ResponseEntity<Resource> getSignedFile(String serviceId, MultipartFile file) {
        byte[] signature = signService.getSigned(getServiceConfiguration(serviceId).getKeyStoreUnlocker(),
                getBytesFromRequestFile(file));

        return getOkByteResponse(signature);
    }

    @Override
    public ResponseEntity<Void> saveKeyStore(String serviceId, DTOStore body) {
        log.info("Saving key store for {} service", serviceId);
        keyStoreService.saveKeyStoreAndUpdateServiceConfigs(getServiceConfiguration(serviceId), body);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> saveTrustStore(String serviceId, DTOStore body) {
        log.info("Saving trust store for {} service", serviceId);
        trustStoreService.saveTrustStoreAndUpdateServiceConfigs(getServiceConfiguration(serviceId), body);
        return ResponseEntity.ok().build();
    }

    private byte[] getBytesFromRequestFile(MultipartFile multipartFile) {
        try {
            return multipartFile.getBytes();
        } catch (IOException e) {
            log.error("Error while got bytes from file", e);
            throw new IllegalStateException("Error while got bytes from file", e);
        }
    }

    private ResponseEntity<Resource> getOkByteResponse(byte[] body) {
        return ResponseEntity.ok(new ByteArrayResource(body));
    }

    private ServiceConfiguration getServiceConfiguration(String serviceId) {
        return serviceConfigurationMap.get(Service.fromValue(serviceId));
    }
}
