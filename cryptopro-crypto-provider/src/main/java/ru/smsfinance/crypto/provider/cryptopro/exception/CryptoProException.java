package ru.smsfinance.crypto.provider.cryptopro.exception;

/**
 * CryptoPro provider exception class
 */
public class CryptoProException extends RuntimeException {

    /**
     * Constructor with exception message
     *
     * @param msg exception message
     */
    public CryptoProException(String msg) {
        super(msg);
    }

    /**
     * @param message exception message
     * @param cause cause
     */
    public CryptoProException(String message, Throwable cause) {
        super(message, cause);
    }
}
