package ru.smsfinance.crypto.provider.cryptopro.service;

import ru.smsfinance.crypto.api.dto.DTOStore;
import ru.smsfinance.crypto.core.keystore.ServiceConfiguration;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;

import java.security.KeyStore;

/**
 * Trust store service
 */
public interface TrustStoreService {

    /**
     * Save trust store on file system and update service configs
     *
     * @param trustStore           request DTO
     * @param serviceConfiguration service configuration
     */
    void saveTrustStoreAndUpdateServiceConfigs(ServiceConfiguration serviceConfiguration, DTOStore trustStore);

    /**
     * Save trust store on file system
     *
     * @param trustStore request DTO
     */
    void saveTrustStore(DTOStore trustStore);

    /**
     * Get trust store from file
     *
     * @param storeUnlocker trust store unlocker
     * @return trust store instance
     */
    KeyStore getTrustStore(StoreUnlocker storeUnlocker);
}
