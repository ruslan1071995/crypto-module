package ru.smsfinance.crypto.provider.cryptopro.model.enums;

/**
 * Signature type
 */
public enum SignatureType {
    LOW_SIGN("CMS сообщение с подписью на хэш данных"),
    SF_SIGN("CMS сообщение с подписью на хэш signedAttributes");

    private final String description;

    SignatureType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
