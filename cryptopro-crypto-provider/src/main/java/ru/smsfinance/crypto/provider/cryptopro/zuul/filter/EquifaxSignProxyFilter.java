package ru.smsfinance.crypto.provider.cryptopro.zuul.filter;

import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.filters.route.SimpleHostRoutingFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import ru.smsfinance.crypto.provider.cryptopro.config.service.EquifaxConfig;
import ru.smsfinance.crypto.provider.cryptopro.exception.CryptoProException;
import ru.smsfinance.crypto.provider.cryptopro.service.SignService;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PROXY_KEY;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.SIMPLE_HOST_ROUTING_FILTER_ORDER;

/**
 * Equifax zuul ROUTE filter for setting signed request body
 */
@Component
@Slf4j
public class EquifaxSignProxyFilter extends SimpleHostRoutingFilter {

    private SignService signService;
    private EquifaxConfig equifaxConfig;

    private static final String EQUIFAX_PROXY_KEY = "equifaxSignedSend";

    /**
     * Autowiring constructor
     *
     * @param helper        zuul proxy request helper
     * @param properties    zuul properties
     * @param signService   sign service
     * @param equifaxConfig equifax service configuration
     */
    @Autowired
    public EquifaxSignProxyFilter(ProxyRequestHelper helper, ZuulProperties properties, SignService signService,
                                  EquifaxConfig equifaxConfig) {
        super(helper, properties);
        this.signService = signService;
        this.equifaxConfig = equifaxConfig;
    }

    @Override
    public int filterOrder() {
        return SIMPLE_HOST_ROUTING_FILTER_ORDER - equifaxConfig.getFilterOrder();
    }

    @Override
    public boolean shouldFilter() {
        String proxyKey = RequestContext.getCurrentContext().get(PROXY_KEY).toString();
        return EQUIFAX_PROXY_KEY.equals(proxyKey);
    }

    @Override
    public Object run() {
        super.run();
        // This need for prevent SimpleHosteRouteFilter running
        RequestContext.getCurrentContext().setRouteHost(null);
        return null;
    }

    @Override
    protected HttpRequest buildHttpRequest(String verb, String uri, InputStreamEntity entity,
                                           MultiValueMap<String, String> headers, MultiValueMap<String, String> params,
                                           HttpServletRequest request) {
        InputStreamEntity signedEntity;

        try {
            if (checkIsEmpty(entity.getContent())) {
                log.debug("Empty body for URI: {}", uri);
                return super.buildHttpRequest(verb, uri, entity, headers, params, request);
            }

            byte[] signed = signService.getSigned(equifaxConfig.getKeyStoreUnlocker(),
                    IOUtils.toByteArray(entity.getContent()));

            ContentType contentType = null;

            if (request.getContentType() != null) {
                contentType = ContentType.parse(request.getContentType());
            }

            signedEntity = new InputStreamEntity(new ByteArrayInputStream(signed), contentType);
        } catch (IOException e) {
            log.error("Error reading body entity", e);
            throw new CryptoProException("Error reading body entity", e);
        }

        return super.buildHttpRequest(verb, uri, signedEntity, headers, params, request);
    }

    private boolean checkIsEmpty(InputStream content) throws IOException {
        return content.available() == 0;
    }
}
