package ru.smsfinance.crypto.provider.cryptopro.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smsfinance.crypto.core.keystore.StoreUnlocker;
import ru.smsfinance.crypto.provider.cryptopro.exception.CryptoProException;
import ru.smsfinance.crypto.provider.cryptopro.service.KeyStoreService;
import ru.smsfinance.crypto.provider.cryptopro.service.SSLService;
import ru.smsfinance.crypto.provider.cryptopro.service.TrustStoreService;
import ru.smsfinance.crypto.provider.cryptopro.tools.KeyStoreTools;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

/**
 * SSL service implementation
 */
@Service
@Slf4j
public class SSLServiceImpl implements SSLService {

    private TrustStoreService trustStoreService;

    private KeyStoreService keyStoreService;

    /**
     * Autowiring constructor
     *
     * @param trustStoreService trust store service
     * @param keyStoreService   key store service
     */
    @Autowired
    public SSLServiceImpl(TrustStoreService trustStoreService, KeyStoreService keyStoreService) {
        this.trustStoreService = trustStoreService;
        this.keyStoreService = keyStoreService;
    }

    @Override
    public SSLContext getInitializedSSLContext(StoreUnlocker keyStoreUnlocker, StoreUnlocker trustStoreUnlocker) {
        SSLContext sslContext = getSSLContextInstance();
        initializeSSLContext(sslContext, keyStoreUnlocker, trustStoreUnlocker);
        log.info("Successfully constructed SSL context for keyId: {}", keyStoreUnlocker.getAlias());
        return sslContext;
    }

    @Override
    public SSLContext getSSLContextInstance() {
        try {
            return SSLContext.getInstance("GostTLS");
        } catch (NoSuchAlgorithmException e) {
            String msg = "Error while SSLContext get instance with GostTLS protocol";
            log.error(msg, e);
            throw new CryptoProException(msg, e);
        }
    }

    @Override
    public void initializeSSLContext(SSLContext sslContext, StoreUnlocker keyStoreUnlocker,
                                     StoreUnlocker trustStoreUnlocker) {
        try {
            KeyManagerFactory keyManagerFactory = constructKeyManagerFactory(keyStoreUnlocker);
            // Trust manager factory can be null
            final TrustManagerFactory trustManagerFactory = constructTrustManagerFactory(trustStoreUnlocker);
            sslContext.init(keyManagerFactory.getKeyManagers(),
                    trustManagerFactory == null ? null : trustManagerFactory.getTrustManagers(),
                    null);
        } catch (Exception e) {
            String msg = "Error while initializing SSLContext";
            log.error(msg, e);
            throw new CryptoProException(msg, e);
        }

    }

    private KeyManagerFactory constructKeyManagerFactory(StoreUnlocker unlocker) throws Exception {
        log.debug("START constructing KeyMangerFactory for keyId: {}", unlocker.getAlias());
        KeyStore keyStore = keyStoreService.copyKeyStoreToMemory(KeyStoreTools.loadAndGetKeyStore(),
                unlocker);
        final KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(
                KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, unlocker.getKeyPassword());
        log.debug("FINISH constructing KeyMangerFactory for keyId: {}", unlocker.getAlias());
        return keyManagerFactory;
    }

    private TrustManagerFactory constructTrustManagerFactory(StoreUnlocker trustStoreUnlocker) throws Exception {
        log.debug("START constructing TrustManagerFactory for keyId: {}", trustStoreUnlocker.getAlias());
        KeyStore originalTrustStore = trustStoreService.getTrustStore(trustStoreUnlocker);

        if (originalTrustStore == null) {
            log.info("Not found trust store for keyId: {}. Use DEFAULT trust store", trustStoreUnlocker.getAlias());
            return null;
        }

        final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(originalTrustStore);
        log.debug("FINISH constructing TrustManagerFactory for keyId: {}", trustStoreUnlocker.getAlias());
        return trustManagerFactory;
    }
}
