package ru.smsfinance.crypto.provider.cryptopro.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.message.BasicHttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.util.ZuulRuntimeException;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import ru.smsfinance.crypto.provider.cryptopro.config.service.NbkiConfig;
import ru.smsfinance.crypto.provider.cryptopro.event.UpdateNbkiSettingsEvent;
import ru.smsfinance.crypto.provider.cryptopro.exception.CryptoProException;
import ru.smsfinance.crypto.provider.cryptopro.service.SSLService;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PROXY_KEY;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.ROUTE_TYPE;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.SIMPLE_HOST_ROUTING_FILTER_ORDER;


/**
 * Nbki ssl proxy filter
 */
@Component
@Slf4j
public class NbkiSslProxyFilter extends ZuulFilter {
    private final boolean forceOriginalQueryStringEncoding;
    private ProxyRequestHelper helper;
    private NbkiConfig nbkiConfig;
    private SSLService sslService;
    private CloseableHttpClient httpClient;
    private volatile boolean isSSLContextInitialized = false;

    private SSLContext sslContext;

    private static final String NBKI_SSL_PROXY_KEY = "nbkiSsl";

    /**
     * Autowiring constructor
     *
     * @param helper     zuul proxy request helper
     * @param properties zuul properties
     * @param nbkiConfig nbki configurations
     * @param sslService ssl service
     */
    @Autowired
    public NbkiSslProxyFilter(ProxyRequestHelper helper, ZuulProperties properties,
                              NbkiConfig nbkiConfig, SSLService sslService) {
        this.helper = helper;
        this.nbkiConfig = nbkiConfig;
        this.sslService = sslService;
        this.forceOriginalQueryStringEncoding = properties
                .isForceOriginalQueryStringEncoding();
        this.sslContext = sslService.getSSLContextInstance();
    }

    /**
     * Initialize http client
     */
    @PostConstruct
    public void initialize() {
        updateHttpClient();
    }

    /**
     * NBKI settings update listener
     *
     * @param event update event
     */
    @EventListener
    public void onUpdateSettings(UpdateNbkiSettingsEvent event) {
        updateHttpClient();
    }

    @Override
    public String filterType() {
        return ROUTE_TYPE;
    }

    @Override
    public int filterOrder() {
        return SIMPLE_HOST_ROUTING_FILTER_ORDER - nbkiConfig.getFilterOrder();
    }

    @Override
    public boolean shouldFilter() {
        String proxyKey = RequestContext.getCurrentContext().get(PROXY_KEY).toString();
        return NBKI_SSL_PROXY_KEY.equals(proxyKey);
    }

    @Override
    public Object run() {
        RequestContext context = RequestContext.getCurrentContext();
        HttpServletRequest request = context.getRequest();
        MultiValueMap<String, String> headers = this.helper
                .buildZuulRequestHeaders(request);
        MultiValueMap<String, String> params = this.helper
                .buildZuulRequestQueryParams(request);
        String verb = getVerb(request);
        InputStream requestEntity = getRequestBody(request);
        if (request.getContentLength() < 0) {
            context.setChunkedRequestBody();
        }

        String uri = this.helper.buildZuulRequestURI(request);
        this.helper.addIgnoredHeaders();

        try {
            CloseableHttpResponse response = forward(getHttpClient(), verb, uri, request,
                    headers, params, requestEntity);
            setResponse(response);
        } catch (Exception ex) {
            throw new ZuulRuntimeException(ex);
        }
        // This needs to prevent SimplehostRoutingFilter execution
        RequestContext.getCurrentContext().setRouteHost(null);
        return null;
    }

    private void updateHttpClient() {
        tryInitializeSSLContext();
        if (isSSLContextInitialized) {
            this.httpClient = HttpClients.custom().setSSLContext(this.sslContext).build();
        }
    }

    private void tryInitializeSSLContext() {
        log.info("Try initialize SSLContext for NBKI");
        isSSLContextInitialized = false;
        try {
            this.sslContext = sslService.getSSLContextInstance();
            sslService.initializeSSLContext(this.sslContext,
                    nbkiConfig.getKeyStoreUnlocker(), nbkiConfig.getTrustStoreUnlocker());
            isSSLContextInitialized = true;
            log.info("Successful initialized SSLContext for NBKI");
        } catch (Exception e) {
            log.error("Error while initialize SSLContext for NBKI ssl filter!");
        }
    }

    private CloseableHttpClient getHttpClient() {
        if (!isSSLContextInitialized) {
            log.error("SSLContext not initialized for NBKI");
            throw new CryptoProException("NBKI SSL context not initialized!");
        }
        return httpClient;
    }

    private CloseableHttpResponse forward(CloseableHttpClient httpclient, String verb,
                                          String uri, HttpServletRequest request, MultiValueMap<String, String> headers,
                                          MultiValueMap<String, String> params, InputStream requestEntity)
            throws Exception {
        Map<String, Object> info = this.helper.debug(verb, uri, headers, params,
                requestEntity);
        URL host = RequestContext.getCurrentContext().getRouteHost();
        HttpHost httpHost = getHttpHost(host);
        uri = StringUtils.cleanPath((host.getPath() + uri).replaceAll("/{2,}", "/"));
        int contentLength = request.getContentLength();

        ContentType contentType = null;

        if (request.getContentType() != null) {
            contentType = ContentType.parse(request.getContentType());
        }

        InputStreamEntity entity = new InputStreamEntity(requestEntity, contentLength, contentType);

        HttpRequest httpRequest = buildHttpRequest(verb, uri, entity, headers, params, request);

        log.debug(httpHost.getHostName() + " " + httpHost.getPort() + " " + httpHost.getSchemeName());

        CloseableHttpResponse zuulResponse = forwardRequest(httpclient, httpHost,
                httpRequest);
        this.helper.appendDebug(info, zuulResponse.getStatusLine().getStatusCode(),
                revertHeaders(zuulResponse.getAllHeaders()));
        return zuulResponse;
    }

    private CloseableHttpResponse forwardRequest(CloseableHttpClient httpclient,
                                                 HttpHost httpHost, HttpRequest httpRequest) throws IOException {
        return httpclient.execute(httpHost, httpRequest);
    }

    private HttpRequest buildHttpRequest(String verb, String uri,
                                         InputStreamEntity entity, MultiValueMap<String, String> headers,
                                         MultiValueMap<String, String> params, HttpServletRequest request) {
        HttpRequest httpRequest;
        String uriWithQueryString = uri + (this.forceOriginalQueryStringEncoding
                ? getEncodedQueryString(request) : this.helper.getQueryString(params));

        switch (verb.toUpperCase()) {
            case "POST":
                HttpPost httpPost = new HttpPost(uriWithQueryString);
                httpRequest = httpPost;
                httpPost.setEntity(entity);
                break;
            case "PUT":
                HttpPut httpPut = new HttpPut(uriWithQueryString);
                httpRequest = httpPut;
                httpPut.setEntity(entity);
                break;
            case "PATCH":
                HttpPatch httpPatch = new HttpPatch(uriWithQueryString);
                httpRequest = httpPatch;
                httpPatch.setEntity(entity);
                break;
            case "DELETE":
                BasicHttpEntityEnclosingRequest entityRequest = new BasicHttpEntityEnclosingRequest(
                        verb, uriWithQueryString);
                httpRequest = entityRequest;
                entityRequest.setEntity(entity);
                break;
            default:
                httpRequest = new BasicHttpRequest(verb, uriWithQueryString);
                log.debug(uriWithQueryString);
        }

        httpRequest.setHeaders(convertHeaders(headers));
        return httpRequest;
    }

    private HttpHost getHttpHost(URL host) {
        return new HttpHost(host.getHost(), host.getPort(),
                host.getProtocol());
    }

    private String getEncodedQueryString(HttpServletRequest request) {
        String query = request.getQueryString();
        return (query != null) ? "?" + query : "";
    }

    private Header[] convertHeaders(MultiValueMap<String, String> headers) {
        List<Header> list = new ArrayList<>();
        headers.keySet().forEach(name -> {
            for (String value : headers.get(name)) {
                list.add(new BasicHeader(name, value));
            }
        });
        return list.toArray(new BasicHeader[0]);
    }

    private InputStream getRequestBody(HttpServletRequest request) {
        InputStream requestEntity = null;
        try {
            requestEntity = request.getInputStream();
        } catch (IOException ex) {
            // no requestBody is ok.
        }
        return requestEntity;
    }

    private String getVerb(HttpServletRequest request) {
        String sMethod = request.getMethod();
        return sMethod.toUpperCase();
    }

    private MultiValueMap<String, String> revertHeaders(Header[] headers) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        for (Header header : headers) {
            String name = header.getName();
            if (!map.containsKey(name)) {
                map.put(name, new ArrayList<>());
            }
            map.get(name).add(header.getValue());
        }
        return map;
    }

    private void setResponse(HttpResponse response) throws IOException {
        RequestContext.getCurrentContext().set("zuulResponse", response);
        this.helper.setResponse(response.getStatusLine().getStatusCode(),
                response.getEntity() == null ? null : response.getEntity().getContent(),
                revertHeaders(response.getAllHeaders()));
    }
}
