package ru.smsfinance.crypto.provider.cryptopro.tools;

import com.objsys.asn1j.runtime.Asn1BerDecodeBuffer;
import com.objsys.asn1j.runtime.Asn1BerEncodeBuffer;
import com.objsys.asn1j.runtime.Asn1Null;
import com.objsys.asn1j.runtime.Asn1ObjectIdentifier;
import com.objsys.asn1j.runtime.Asn1OctetString;
import com.objsys.asn1j.runtime.Asn1Type;
import com.objsys.asn1j.runtime.Asn1UTCTime;
import ru.CryptoPro.JCP.ASN.CryptographicMessageSyntax.*;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Attribute;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Attribute_values;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.CertificateSerialNumber;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Name;
import ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Time;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCP.params.OID;
import ru.smsfinance.crypto.provider.cryptopro.model.enums.SignatureType;

import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Calendar;

/**
 * Sign utils class
 */
public final class SignTools {

    private SignTools() {
    }

    public static byte[] getSigned(byte[] message, SignatureType signType,
                                   PrivateKey privateKey, Certificate certificate, boolean detached) throws Exception {

        // that need for backward compatibility
        PrivateKey[] privateKeys = {privateKey};
        Certificate[] certificates = {certificate};


        byte[] signedMessage = null;
        if (privateKey == null || certificate == null) {
            throw new IllegalArgumentException("Signature failed. Key or certificate was not found.");
        }

        //Sign (create CMS or sign CMS)
        boolean isCMS = true;
        final Asn1BerDecodeBuffer asnBuf = new Asn1BerDecodeBuffer(message);
        final ContentInfo all = new ContentInfo();
        try {
            all.decode(asnBuf);
        } catch (Exception e) {
            //создаем новое cms сообщение
            if (signType == SignatureType.LOW_SIGN) {
                KeyStoreTools.logger.info("Create CMS (low)");
                signedMessage = createCMS(message, privateKeys, certificates, detached);
            } else { //SignatureType.SF_SIGN
                KeyStoreTools.logger.info("Create CMS (sf)");
                signedMessage = createhashCMS(message, privateKeys, certificates, detached);
            }
            isCMS = false;
        }
        if (isCMS) {
            //подписываем уже созданное cms сообщение
            if (signType == SignatureType.LOW_SIGN) {
                KeyStoreTools.logger.info("Sign CMS (low)");
                signedMessage = signCMS(message, privateKeys, certificates, null);
            } else { //SignatureType.SF_SIGN
                KeyStoreTools.logger.info("Sign CMS (sf)");
                signedMessage = hashsignCMS(message, privateKeys, certificates, null);
            }
        }
        return signedMessage;
    }

    /**
     * Создание сообщения с подписью на хэш signedAttributes
     *
     * @param data     data
     * @param keys     keys
     * @param certs    certs
     * @param detached true if detached
     * @return byte[]
     * @throws Exception e
     */
    private static byte[] createhashCMS(byte[] data, PrivateKey[] keys,
                                        Certificate[] certs, boolean detached)
            throws Exception {
        //create hashCMS
        final ContentInfo all = new ContentInfo();
        all.contentType =
                new Asn1ObjectIdentifier(
                        new OID(KeyStoreTools.STR_CMS_OID_SIGNED).value);
        final SignedData cms = new SignedData();
        all.content = cms;
        cms.version = new CMSVersion(1);
        // digest
        cms.digestAlgorithms = new DigestAlgorithmIdentifiers(1);
        final DigestAlgorithmIdentifier a = new DigestAlgorithmIdentifier(
                new OID(KeyStoreTools.DIGEST_OID).value);
        a.parameters = new Asn1Null();
        cms.digestAlgorithms.elements[0] = a;
        if (detached)
            cms.encapContentInfo = new EncapsulatedContentInfo(
                    new Asn1ObjectIdentifier(
                            new OID(KeyStoreTools.STR_CMS_OID_DATA).value),
                    null);
        else
            cms.encapContentInfo =
                    new EncapsulatedContentInfo(new Asn1ObjectIdentifier(
                            new OID(KeyStoreTools.STR_CMS_OID_DATA).value),
                            new Asn1OctetString(data));
        // certificates
        final int ncerts = certs.length;
        cms.certificates = new CertificateSet(ncerts);
        cms.certificates.elements = new CertificateChoices[ncerts];
        for (int i = 0; i < cms.certificates.elements.length; i++) {
            final ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate certificate =
                    new ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate();
            final Asn1BerDecodeBuffer decodeBuffer =
                    new Asn1BerDecodeBuffer(certs[i].getEncoded());
            certificate.decode(decodeBuffer);
            cms.certificates.elements[i] = new CertificateChoices();
            cms.certificates.elements[i].set_certificate(certificate);
        }
        // TODO: dynamic get signature instance and set signer infos
        // Signature.getInstance
        final Signature signature =
                Signature.getInstance(JCP.GOST_EL_SIGN_NAME);
        byte[] sign;
        // signer infos
        final int nsign = keys.length;
        cms.signerInfos = new SignerInfos(nsign);
        for (int i = 0; i < cms.signerInfos.elements.length; i++) {
            cms.signerInfos.elements[i] = new SignerInfo();
            cms.signerInfos.elements[i].version = new CMSVersion(1);
            cms.signerInfos.elements[i].sid = new SignerIdentifier();

            final byte[] encodedName =
                    ((X509Certificate) certs[i]).getIssuerX500Principal()
                            .getEncoded();
            final Asn1BerDecodeBuffer nameBuf =
                    new Asn1BerDecodeBuffer(encodedName);
            final Name name = new Name();
            name.decode(nameBuf);

            final CertificateSerialNumber num = new CertificateSerialNumber(
                    ((X509Certificate) certs[i]).getSerialNumber());
            cms.signerInfos.elements[i].sid.set_issuerAndSerialNumber(
                    new IssuerAndSerialNumber(name, num));
            cms.signerInfos.elements[i].digestAlgorithm =
                    new DigestAlgorithmIdentifier(
                            new OID(KeyStoreTools.DIGEST_OID).value);
            cms.signerInfos.elements[i].digestAlgorithm.parameters = new Asn1Null();
            cms.signerInfos.elements[i].signatureAlgorithm =
                    new SignatureAlgorithmIdentifier(
                            new OID(KeyStoreTools.SIGN_OID).value);
            cms.signerInfos.elements[i].signatureAlgorithm.parameters =
                    new Asn1Null();

            //signedAttributes
            final int kmax = 3;
            cms.signerInfos.elements[i].signedAttrs = new SignedAttributes(kmax);

            //-contentType
            int k = 0;
            cms.signerInfos.elements[i].signedAttrs.elements[k] =
                    new Attribute(new OID(KeyStoreTools.STR_CMS_OID_CONT_TYP_ATTR).value,
                            new Attribute_values(1));

            final Asn1Type conttype = new Asn1ObjectIdentifier(
                    new OID(KeyStoreTools.STR_CMS_OID_DATA).value);

            cms.signerInfos.elements[i].signedAttrs.elements[k].values.elements[0] =
                    conttype;

            k += 1;
            cms.signerInfos.elements[i].signedAttrs.elements[k] =
                    new Attribute(new OID(KeyStoreTools.STR_CMS_OID_SIGN_TYM_ATTR).value,
                            new Attribute_values(1));

            final Time time = new Time();

            final Asn1UTCTime UTCTime = new Asn1UTCTime();
            //текущая дата с календаря
            UTCTime.setTime(Calendar.getInstance());
            time.set_utcTime(UTCTime);

            cms.signerInfos.elements[i].signedAttrs.elements[k].values.elements[0] =
                    time.getElement();

            //-message digest
            k += 1;
            cms.signerInfos.elements[i].signedAttrs.elements[k] =
                    new Attribute(new OID(KeyStoreTools.STR_CMS_OID_DIGEST_ATTR).value,
                            new Attribute_values(1));
            final byte[] messagedigestb;
            if (detached)
                messagedigestb = KeyStoreTools.digestm(data, KeyStoreTools.DIGEST_ALG_NAME);
            else
                messagedigestb =
                        KeyStoreTools.digestm(cms.encapContentInfo.eContent.value,
                                KeyStoreTools.DIGEST_ALG_NAME);
            final Asn1Type messagedigest = new Asn1OctetString(messagedigestb);

            cms.signerInfos.elements[i].signedAttrs.elements[k].values.elements[0] =
                    messagedigest;

            //signature
            Asn1BerEncodeBuffer encBufSignedAttr = new Asn1BerEncodeBuffer();
            cms.signerInfos.elements[i].signedAttrs
                    .encode(encBufSignedAttr);
            final byte[] hsign = encBufSignedAttr.getMsgCopy();
            signature.initSign(keys[i]);
            signature.update(hsign);
            sign = signature.sign();

            cms.signerInfos.elements[i].signature = new SignatureValue(sign);
        }
        // encode
        final Asn1BerEncodeBuffer asnBuf = new Asn1BerEncodeBuffer();
        all.encode(asnBuf, true);
        //Array.writeFile(path, asnBuf.getMsgCopy()); вот так можно записать в файл
        return asnBuf.getMsgCopy();

    }

    /**
     * Создание сообщение с подписью на хэш данных
     *
     * @param data     data
     * @param certs    certs[]
     * @param keys     keys
     * @param detached true if detached
     * @return byte[]
     * @throws Exception e
     */
    private static byte[] createCMS(byte[] data, PrivateKey[] keys,
                                    Certificate[] certs, boolean detached)
            throws Exception {
        //create CMS
        final ContentInfo all = new ContentInfo();
        all.contentType =
                new Asn1ObjectIdentifier(
                        new OID(KeyStoreTools.STR_CMS_OID_SIGNED).value);
        final SignedData cms = new SignedData();
        all.content = cms;
        cms.version = new CMSVersion(1);
        // digest
        cms.digestAlgorithms = new DigestAlgorithmIdentifiers(1);
        final DigestAlgorithmIdentifier a = new DigestAlgorithmIdentifier(
                new OID(KeyStoreTools.DIGEST_OID).value);
        a.parameters = new Asn1Null();
        cms.digestAlgorithms.elements[0] = a;
        if (detached)
            cms.encapContentInfo = new EncapsulatedContentInfo(
                    new Asn1ObjectIdentifier(
                            new OID(KeyStoreTools.STR_CMS_OID_DATA).value),
                    null);
        else
            cms.encapContentInfo =
                    new EncapsulatedContentInfo(new Asn1ObjectIdentifier(
                            new OID(KeyStoreTools.STR_CMS_OID_DATA).value),
                            new Asn1OctetString(data));
        // certificates
        final int ncerts = certs.length;
        cms.certificates = new CertificateSet(ncerts);
        cms.certificates.elements = new CertificateChoices[ncerts];
        for (int i = 0; i < cms.certificates.elements.length; i++) {
            final ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate certificate =
                    new ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate();
            final Asn1BerDecodeBuffer decodeBuffer =
                    new Asn1BerDecodeBuffer(certs[i].getEncoded());
            certificate.decode(decodeBuffer);
            cms.certificates.elements[i] = new CertificateChoices();
            cms.certificates.elements[i].set_certificate(certificate);
        }
        // Signature.getInstance
        final Signature signature =
                Signature.getInstance(JCP.GOST_EL_SIGN_NAME);
        byte[] sign;
        // signer infos
        final int nsign = keys.length;
        cms.signerInfos = new SignerInfos(nsign);
        for (int i = 0; i < cms.signerInfos.elements.length; i++) {
            signature.initSign(keys[i]);
            signature.update(data);
            sign = signature.sign();
            cms.signerInfos.elements[i] = new SignerInfo();
            cms.signerInfos.elements[i].version = new CMSVersion(1);
            cms.signerInfos.elements[i].sid = new SignerIdentifier();

            final byte[] encodedName =
                    ((X509Certificate) certs[i]).getIssuerX500Principal()
                            .getEncoded();
            final Asn1BerDecodeBuffer nameBuf =
                    new Asn1BerDecodeBuffer(encodedName);
            final Name name = new Name();
            name.decode(nameBuf);

            final CertificateSerialNumber num = new CertificateSerialNumber(
                    ((X509Certificate) certs[i]).getSerialNumber());
            cms.signerInfos.elements[i].sid.set_issuerAndSerialNumber(
                    new IssuerAndSerialNumber(name, num));
            cms.signerInfos.elements[i].digestAlgorithm =
                    new DigestAlgorithmIdentifier(
                            new OID(KeyStoreTools.DIGEST_OID).value);
            cms.signerInfos.elements[i].digestAlgorithm.parameters = new Asn1Null();
            cms.signerInfos.elements[i].signatureAlgorithm =
                    new SignatureAlgorithmIdentifier(
                            new OID(KeyStoreTools.SIGN_OID).value);
            cms.signerInfos.elements[i].signatureAlgorithm.parameters =
                    new Asn1Null();
            cms.signerInfos.elements[i].signature = new SignatureValue(sign);
        }
        // encode
        final Asn1BerEncodeBuffer asnBuf = new Asn1BerEncodeBuffer();
        all.encode(asnBuf, true);
//        Array.writeFile(path, asnBuf.getMsgCopy());
        return asnBuf.getMsgCopy();
    }

    /**
     * Подпись существующего сообщения (CMS) //хэш на данные
     *
     * @param buffer CMS
     * @param keys   keys
     * @param certs  certs
     * @param data   data if detached signature
     * @return byte[]
     * @throws Exception e
     */
    private static byte[] signCMS(byte[] buffer, PrivateKey[] keys,
                                  Certificate[] certs, byte[] data)
            throws Exception {
        int i;
        final Asn1BerDecodeBuffer asnBuf = new Asn1BerDecodeBuffer(buffer);
        final ContentInfo all = new ContentInfo();
        all.decode(asnBuf);
        if (!new OID(KeyStoreTools.STR_CMS_OID_SIGNED).eq(all.contentType.value))
            throw new Exception("Not supported");
        final SignedData cms = (SignedData) all.content;
        if (cms.version.value != 1)
            throw new Exception("Incorrect version");
        if (!new OID(KeyStoreTools.STR_CMS_OID_DATA).eq(
                cms.encapContentInfo.eContentType.value))
            throw new Exception("Nested not supported");
        final byte[] text;
        if (cms.encapContentInfo.eContent != null)
            text = cms.encapContentInfo.eContent.value;
        else if (data != null) text = data;
        else throw new Exception("No content");
//    final byte[] text = cms.encapContentInfo.eContent.value;
        OID digestOid = null;
        final DigestAlgorithmIdentifier a = new DigestAlgorithmIdentifier(
                new OID(KeyStoreTools.DIGEST_OID).value);
        for (i = 0; i < cms.digestAlgorithms.elements.length; i++) {
            if (cms.digestAlgorithms.elements[i].algorithm.equals(a.algorithm)) {
                digestOid =
                        new OID(cms.digestAlgorithms.elements[i].algorithm.value);
                break;
            }
        }
        if (digestOid == null)
            throw new Exception("Unknown digest");
        // certificates
        final CertificateChoices[] choiceses =
                new CertificateChoices[cms.certificates.elements.length];
        for (i = 0; i < cms.certificates.elements.length; i++) {
            choiceses[i] = cms.certificates.elements[i];
        }
        final int ncerts = certs.length + choiceses.length;
        cms.certificates = new CertificateSet(ncerts);
        cms.certificates.elements = new CertificateChoices[ncerts];
        for (i = 0; i < choiceses.length; i++) {
            cms.certificates.elements[i] = choiceses[i];
        }
        for (i = 0; i < cms.certificates.elements.length - choiceses.length; i++) {
            final ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate certificate =
                    new ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate();
            final Asn1BerDecodeBuffer decodeBuffer =
                    new Asn1BerDecodeBuffer(certs[i].getEncoded());
            certificate.decode(decodeBuffer);
            cms.certificates.elements[i + choiceses.length] =
                    new CertificateChoices();
            cms.certificates.elements[i + choiceses.length]
                    .set_certificate(certificate);
        }
        // Signature.getInstance
        final Signature signature =
                Signature.getInstance(JCP.GOST_EL_SIGN_NAME);
        byte[] sign;
        // signer infos
        final SignerInfo[] infos = new SignerInfo[cms.signerInfos.elements.length];
        for (i = 0; i < cms.signerInfos.elements.length; i++) {
            infos[i] = cms.signerInfos.elements[i];
        }
        final int nsign = keys.length + infos.length;
        cms.signerInfos = new SignerInfos(nsign);
        for (i = 0; i < infos.length; i++) {
            cms.signerInfos.elements[i] = infos[i];
        }
        for (i = 0; i < cms.signerInfos.elements.length - infos.length; i++) {
            signature.initSign(keys[i]);
            signature.update(text);
            sign = signature.sign();
            cms.signerInfos.elements[i + infos.length] = new SignerInfo();
            cms.signerInfos.elements[i + infos.length].version = new CMSVersion(1);
            cms.signerInfos.elements[i + infos.length].sid = new SignerIdentifier();

            final byte[] encodedName =
                    ((X509Certificate) certs[i]).getIssuerX500Principal()
                            .getEncoded();
            final Asn1BerDecodeBuffer nameBuf =
                    new Asn1BerDecodeBuffer(encodedName);
            final Name name = new Name();
            name.decode(nameBuf);

            final CertificateSerialNumber num = new CertificateSerialNumber(
                    ((X509Certificate) certs[i]).getSerialNumber());
            cms.signerInfos.elements[i + infos.length].sid
                    .set_issuerAndSerialNumber(
                            new IssuerAndSerialNumber(name, num));
            cms.signerInfos.elements[i + infos.length].digestAlgorithm =
                    new DigestAlgorithmIdentifier(
                            new OID(KeyStoreTools.DIGEST_OID).value);
            cms.signerInfos.elements[i + infos.length].digestAlgorithm.parameters =
                    new Asn1Null();
            cms.signerInfos.elements[i + infos.length].signatureAlgorithm =
                    new SignatureAlgorithmIdentifier(
                            new OID(KeyStoreTools.SIGN_OID).value);
            cms.signerInfos.elements[i + infos
                    .length].signatureAlgorithm.parameters =
                    new Asn1Null();
            cms.signerInfos.elements[i + infos.length].signature =
                    new SignatureValue(sign);
        }
        // encode
        final Asn1BerEncodeBuffer asn1Buf = new Asn1BerEncodeBuffer();
        all.encode(asn1Buf, true);
        //Array.writeFile(path, asn1Buf.getMsgCopy());
        return asn1Buf.getMsgCopy();
    }

    /**
     * Подпись существующего сообщения (CMS) //хэш на signedAttributes
     *
     * @param buffer CMS
     * @param keys   keys
     * @param certs  certs
     * @param data   data if detached signature
     * @return byte[]
     * @throws Exception e
     */
    private static byte[] hashsignCMS(byte[] buffer, PrivateKey[] keys,
                                      Certificate[] certs, byte[] data)
            throws Exception {
        int i;
        final Asn1BerDecodeBuffer asnBuf = new Asn1BerDecodeBuffer(buffer);
        final ContentInfo all = new ContentInfo();
        all.decode(asnBuf);
        if (!new OID(KeyStoreTools.STR_CMS_OID_SIGNED).eq(all.contentType.value))
            throw new Exception("Not supported");
        final SignedData cms = (SignedData) all.content;
        if (cms.version.value != 1)
            throw new Exception("Incorrect version");
        if (!new OID(KeyStoreTools.STR_CMS_OID_DATA).eq(
                cms.encapContentInfo.eContentType.value))
            throw new Exception("Nested not supported");
        OID digestOid = null;
        final DigestAlgorithmIdentifier a = new DigestAlgorithmIdentifier(
                new OID(KeyStoreTools.DIGEST_OID).value);
        for (i = 0; i < cms.digestAlgorithms.elements.length; i++) {
            if (cms.digestAlgorithms.elements[i].algorithm.equals(a.algorithm)) {
                digestOid =
                        new OID(cms.digestAlgorithms.elements[i].algorithm.value);
                break;
            }
        }
        if (digestOid == null)
            throw new Exception("Unknown digest");
        // certificates
        final CertificateChoices[] choiceses =
                new CertificateChoices[cms.certificates.elements.length];
        for (i = 0; i < cms.certificates.elements.length; i++) {
            choiceses[i] = cms.certificates.elements[i];
        }
        final int ncerts = certs.length + choiceses.length;
        cms.certificates = new CertificateSet(ncerts);
        cms.certificates.elements = new CertificateChoices[ncerts];
        for (i = 0; i < choiceses.length; i++) {
            cms.certificates.elements[i] = choiceses[i];
        }
        for (i = 0; i < cms.certificates.elements.length - choiceses.length; i++) {
            final ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate certificate =
                    new ru.CryptoPro.JCP.ASN.PKIX1Explicit88.Certificate();
            final Asn1BerDecodeBuffer decodeBuffer =
                    new Asn1BerDecodeBuffer(certs[i].getEncoded());
            certificate.decode(decodeBuffer);
            cms.certificates.elements[i + choiceses.length] =
                    new CertificateChoices();
            cms.certificates.elements[i + choiceses.length]
                    .set_certificate(certificate);
        }
        // Signature.getInstance
        final Signature signature =
                Signature.getInstance(JCP.GOST_EL_SIGN_NAME);
        byte[] sign;
        // signer infos
        final SignerInfo[] infos = new SignerInfo[cms.signerInfos.elements.length];
        for (i = 0; i < cms.signerInfos.elements.length; i++) {
            infos[i] = cms.signerInfos.elements[i];
        }
        final int nsign = keys.length + infos.length;
        cms.signerInfos = new SignerInfos(nsign);
        for (i = 0; i < infos.length; i++) {
            cms.signerInfos.elements[i] = infos[i];
        }
        for (i = 0; i < cms.signerInfos.elements.length - infos.length; i++) {
            cms.signerInfos.elements[i + infos.length] = new SignerInfo();
            cms.signerInfos.elements[i + infos.length].version = new CMSVersion(1);
            cms.signerInfos.elements[i + infos.length].sid = new SignerIdentifier();

            final byte[] encodedName =
                    ((X509Certificate) certs[i]).getIssuerX500Principal()
                            .getEncoded();
            final Asn1BerDecodeBuffer nameBuf =
                    new Asn1BerDecodeBuffer(encodedName);
            final Name name = new Name();
            name.decode(nameBuf);

            final CertificateSerialNumber num = new CertificateSerialNumber(
                    ((X509Certificate) certs[i]).getSerialNumber());
            cms.signerInfos.elements[i + infos.length].sid
                    .set_issuerAndSerialNumber(
                            new IssuerAndSerialNumber(name, num));
            cms.signerInfos.elements[i + infos.length].digestAlgorithm =
                    new DigestAlgorithmIdentifier(
                            new OID(KeyStoreTools.DIGEST_OID).value);
            cms.signerInfos.elements[i + infos.length].digestAlgorithm.parameters =
                    new Asn1Null();
            cms.signerInfos.elements[i + infos.length].signatureAlgorithm =
                    new SignatureAlgorithmIdentifier(
                            new OID(KeyStoreTools.SIGN_OID).value);
            cms.signerInfos.elements[i + infos
                    .length].signatureAlgorithm.parameters =
                    new Asn1Null();
            //signedAttributes
            final int kmax = 3;
            cms.signerInfos.elements[i + infos.length].signedAttrs =
                    new SignedAttributes(kmax);

            //-contentType
            int k = 0;
            cms.signerInfos.elements[i + infos.length].signedAttrs.elements[k] =
                    new Attribute(new OID(KeyStoreTools.STR_CMS_OID_CONT_TYP_ATTR).value,
                            new Attribute_values(1));

            final Asn1Type conttype = new Asn1ObjectIdentifier(
                    new OID(KeyStoreTools.STR_CMS_OID_DATA).value);

            cms.signerInfos.elements[i + infos
                    .length].signedAttrs.elements[k].values.elements[0] =
                    conttype;

            //-Time
            k += 1;
            cms.signerInfos.elements[i + infos.length].signedAttrs.elements[k] =
                    new Attribute(new OID(KeyStoreTools.STR_CMS_OID_SIGN_TYM_ATTR).value,
                            new Attribute_values(1));

            final Time time = new Time();

            final Asn1UTCTime UTCTime = new Asn1UTCTime();
            //текущая дата с календаря
            UTCTime.setTime(Calendar.getInstance());
            time.set_utcTime(UTCTime);

            cms.signerInfos.elements[i + infos
                    .length].signedAttrs.elements[k].values.elements[0] =
                    time.getElement();

            //-message digest
            k += 1;
            cms.signerInfos.elements[i + infos.length].signedAttrs.elements[k] =
                    new Attribute(new OID(KeyStoreTools.STR_CMS_OID_DIGEST_ATTR).value,
                            new Attribute_values(1));
            final byte[] messagedigestb;
            if (cms.encapContentInfo.eContent.value != null)
                messagedigestb =
                        KeyStoreTools.digestm(cms.encapContentInfo.eContent.value,
                                KeyStoreTools.DIGEST_ALG_NAME);
            else if (data != null)
                messagedigestb = KeyStoreTools.digestm(data, KeyStoreTools.DIGEST_ALG_NAME);
            else throw new Exception("No content");

            final Asn1Type messagedigest = new Asn1OctetString(messagedigestb);

            cms.signerInfos.elements[i + infos
                    .length].signedAttrs.elements[k].values.elements[0] =
                    messagedigest;

            //signature
            Asn1BerEncodeBuffer encBufSignedAttr = new Asn1BerEncodeBuffer();
            cms.signerInfos.elements[i + infos.length].signedAttrs
                    .encode(encBufSignedAttr);
            final byte[] hsign = encBufSignedAttr.getMsgCopy();
            signature.initSign(keys[i]);
            signature.update(hsign);
            sign = signature.sign();

            cms.signerInfos.elements[i + infos.length].signature =
                    new SignatureValue(sign);
        }
        // encode
        final Asn1BerEncodeBuffer asn1Buf = new Asn1BerEncodeBuffer();
        all.encode(asn1Buf, true);
        //Array.writeFile(path, asn1Buf.getMsgCopy());
        return asn1Buf.getMsgCopy();
    }

    public static String dropSignature(byte[] buffer) {
        try {
            final Asn1BerDecodeBuffer asnBuf = new Asn1BerDecodeBuffer(buffer);
            final ContentInfo all = new ContentInfo();
            all.decode(asnBuf);

            if (!new OID(KeyStoreTools.STR_CMS_OID_SIGNED).eq(all.contentType.value)) {
                throw new RuntimeException("Content type not supported: " + all.contentType);
            }

            final SignedData cms = (SignedData) all.content;
            byte[] text = null;

            if (cms.encapContentInfo.eContent != null) {
                text = cms.encapContentInfo.eContent.value;
            }

            return new String(text, "windows-1251");
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Error while drop signature", e);
        }
    }
}
