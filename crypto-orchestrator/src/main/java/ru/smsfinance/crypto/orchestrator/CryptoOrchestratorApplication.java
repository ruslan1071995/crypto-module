package ru.smsfinance.crypto.orchestrator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Orchestrator application
 */
@SpringBootApplication
public class CryptoOrchestratorApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CryptoOrchestratorApplication.class);
    }

    /**
     * Start point for run application
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(CryptoOrchestratorApplication.class, args);
    }
}
