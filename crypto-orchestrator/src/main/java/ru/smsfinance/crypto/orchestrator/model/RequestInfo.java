package ru.smsfinance.crypto.orchestrator.model;

import lombok.Data;
import ru.smsfinance.crypto.orchestrator.model.enums.RequestStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Request info. Need for orchestrator logging
 */
@Data
@Entity
@Table(name = "reuquest_info")
public class RequestInfo {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * Requested url
     */
    @Column(name = "url", columnDefinition = "TEXT")
    private String url;

    /**
     * HTTP Method name
     */
    @Column(name = "method")
    private String method;

    /**
     * Request result status
     */
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private RequestStatus requestStatus;

    /**
     * Start time of request
     */
    @Column(name = "request_start_time")
    private LocalDateTime requestStartTime;

    /**
     * Response time for request
     */
    @Column(name = "response_time")
    private LocalDateTime responseTime;
}
