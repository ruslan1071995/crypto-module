package ru.smsfinance.crypto.orchestrator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.smsfinance.crypto.orchestrator.model.RequestInfo;

/**
 * Request info repository
 */
@Repository
public interface RequestInfoRepository extends JpaRepository<RequestInfo, Long> {
}
