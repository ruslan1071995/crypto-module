package ru.smsfinance.crypto.orchestrator.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.stereotype.Component;
import ru.smsfinance.crypto.orchestrator.model.RequestInfo;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

/**
 * Filter that saves start request time to request context
 */
@Component
public class PreLoggingFilter extends ZuulFilter {

    public static final String REQUEST_INFO_KEY = "requestInfo";

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    /**
     * Before {@link org.springframework.cloud.netflix.zuul.filters.pre.PreDecorationFilter}
     */
    @Override
    public int filterOrder() {
        return PRE_DECORATION_FILTER_ORDER - 1;
    }

    /**
     * Always execute
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();

        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setRequestStartTime(LocalDateTime.now());
        requestInfo.setUrl(request.getRequestURL().toString());
        requestInfo.setMethod(request.getMethod());

        currentContext.put(REQUEST_INFO_KEY, requestInfo);
        return null;
    }
}
