package ru.smsfinance.crypto.orchestrator.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import ru.smsfinance.crypto.orchestrator.model.RequestInfo;
import ru.smsfinance.crypto.orchestrator.model.enums.RequestStatus;
import ru.smsfinance.crypto.orchestrator.repository.RequestInfoRepository;

import java.time.LocalDateTime;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.POST_TYPE;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.SEND_RESPONSE_FILTER_ORDER;
import static ru.smsfinance.crypto.orchestrator.zuul.filter.PreLoggingFilter.REQUEST_INFO_KEY;

/**
 * Filter that saves request info to DB
 */
@Component
public class PostLoggingFilter extends ZuulFilter {

    private RequestInfoRepository requestInfoRepository;

    /**
     * Autowiring constructor
     *
     * @param requestInfoRepository request info repository
     */
    @Autowired
    public PostLoggingFilter(RequestInfoRepository requestInfoRepository) {
        this.requestInfoRepository = requestInfoRepository;
    }

    @Override
    public String filterType() {
        return POST_TYPE;
    }

    /**
     * Executes before {@link org.springframework.cloud.netflix.zuul.filters.post.SendResponseFilter}
     */
    @Override
    public int filterOrder() {
        return SEND_RESPONSE_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext currentContext = RequestContext.getCurrentContext();

        RequestInfo requestInfo = (RequestInfo) currentContext.get(REQUEST_INFO_KEY);

        if (currentContext.getThrowable() != null) {
            requestInfo.setRequestStatus(RequestStatus.ERROR);
        }

        setStatus(HttpStatus.valueOf(currentContext.getResponseStatusCode()), requestInfo);

        requestInfo.setResponseTime(LocalDateTime.now());
        requestInfoRepository.saveAndFlush(requestInfo);

        return null;
    }

    private void setStatus(HttpStatus status, RequestInfo requestInfo) {
        if (status.is2xxSuccessful()) {
            requestInfo.setRequestStatus(RequestStatus.OK);
        } else if (status.is4xxClientError()) {
            requestInfo.setRequestStatus(RequestStatus.CLIENT_ERROR);
        } else if (status.is5xxServerError()) {
            requestInfo.setRequestStatus(RequestStatus.SERVER_ERROR);
        }
    }
}
