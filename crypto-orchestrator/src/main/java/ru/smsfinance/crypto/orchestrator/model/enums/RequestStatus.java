package ru.smsfinance.crypto.orchestrator.model.enums;

/**
 * Request status
 */
public enum RequestStatus {
    /**
     * OK (2xx HTTP code)
     */
    OK,
    /**
     * Error on server side (5xx HTTP code)
     */
    SERVER_ERROR,
    /**
     * Error on orchestrator client (4xx HTTP code)
     */
    CLIENT_ERROR,
    /**
     * Unidentified error
     */
    ERROR
}
