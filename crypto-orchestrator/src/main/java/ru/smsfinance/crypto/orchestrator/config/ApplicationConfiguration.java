package ru.smsfinance.crypto.orchestrator.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.smsfinance.crypto.api.config.SwaggerConfig;
import ru.smsfinance.crypto.orchestrator.model.RequestInfo;
import ru.smsfinance.crypto.orchestrator.repository.RequestInfoRepository;

/**
 * Crypto provider configuration bean
 */
@Configuration
@EnableZuulProxy
@EnableJpaRepositories(basePackageClasses = {RequestInfoRepository.class})
@EntityScan(basePackageClasses = {RequestInfo.class, Jsr310JpaConverters.class})
@Import(SwaggerConfig.class)
public class ApplicationConfiguration {
}
