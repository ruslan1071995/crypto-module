package ru.smsfinance.crypto.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.smsfinance.crypto.core.model.LocalKeyStore;

/**
 * Key store repository
 */
@Repository
public interface KeyStoreRepository extends JpaRepository<LocalKeyStore, Long> {

    /**
     * Find last uploaded keystore by alias
     *
     * @param keyId key alias
     * @return key store entity
     */
    LocalKeyStore findFirstByKeyIdOrderByUploadTimeDesc(String keyId);
}
