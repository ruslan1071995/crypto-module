package ru.smsfinance.crypto.core.keystore;

/**
 * This interface for that configs bean which have keyStoreUnlocker
 */
public interface ServiceConfiguration {
    /**
     * Get key store unlocker
     *
     * @return key store unlocker
     */
    StoreUnlocker getKeyStoreUnlocker();

    /**
     * Set key store unlocker
     *
     * @param keyStoreUnlocker key store unlocker
     */
    void setKeyStoreUnlocker(StoreUnlocker keyStoreUnlocker);

    /**
     * Get trust store unlocker
     *
     * @return trust store unlocker
     */
    StoreUnlocker getTrustStoreUnlocker();

    /**
     * Set trust store unlocker
     *
     * @param storeUnlocker trust store unlocker
     */
    void setTrustStoreUnlocker(StoreUnlocker storeUnlocker);
}
