package ru.smsfinance.crypto.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.smsfinance.crypto.core.model.KeyStoreUploadCast;

/**
 * Key store cast repository
 */
@Repository
public interface KeyStoreCastRepository extends JpaRepository<KeyStoreUploadCast, Long> {
}
