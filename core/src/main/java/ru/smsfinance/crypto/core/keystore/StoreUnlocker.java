package ru.smsfinance.crypto.core.keystore;

import lombok.Data;
import lombok.ToString;

/**
 * Private key unlocker object
 */
@Data
@ToString(exclude = {"keyPassword", "storePassword"})
public class StoreUnlocker {
    /**
     * Key alias for getting from store
     */
    private String alias;

    /**
     * Private key password
     */
    private char[] keyPassword;

    /**
     * Key store password
     */
    private char[] storePassword;
}
