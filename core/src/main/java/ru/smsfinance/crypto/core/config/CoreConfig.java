package ru.smsfinance.crypto.core.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.smsfinance.crypto.api.config.SwaggerConfig;
import ru.smsfinance.crypto.core.model.LocalKeyStore;
import ru.smsfinance.crypto.core.repository.KeyStoreRepository;

/**
 * Core configuration
 */
@Configuration
@EnableJpaRepositories(basePackageClasses = {KeyStoreRepository.class})
@EntityScan(basePackageClasses = {LocalKeyStore.class, Jsr310JpaConverters.class})
@Import(SwaggerConfig.class)
public class CoreConfig {
}
