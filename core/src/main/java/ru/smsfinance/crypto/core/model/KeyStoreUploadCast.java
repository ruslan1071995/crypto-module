package ru.smsfinance.crypto.core.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * File cast when key store was uploaded
 */
@Data
@Entity
@Table(name = "key_store_cast")
@ToString(exclude = "file")
public class KeyStoreUploadCast {

    @Id
    @GeneratedValue
    private Long id;

    @Lob
    @Column(name = "file")
    private byte[] file;

    @ManyToOne
    @JoinColumn(name = "key_store_id")
    private LocalKeyStore keyStore;

    @Column(name = "upload_time")
    private LocalDateTime uploadTime = LocalDateTime.now();

}
