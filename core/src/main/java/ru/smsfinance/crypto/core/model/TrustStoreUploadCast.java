package ru.smsfinance.crypto.core.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Trust store cast. Need when uploading trust store for backup purposes
 */
@Entity
@Table(name = "trust_store_cast")
@Data
@ToString(exclude = "file")
public class TrustStoreUploadCast {

    @Id
    @GeneratedValue
    private Long id;

    @Lob
    @Column(name = "file")
    private byte[] file;

    @ManyToOne
    @JoinColumn(name = "trust_store_id")
    private LocalTrustStore trustStore;

    @Column(name = "upload_time")
    private LocalDateTime uploadTime = LocalDateTime.now();
}
