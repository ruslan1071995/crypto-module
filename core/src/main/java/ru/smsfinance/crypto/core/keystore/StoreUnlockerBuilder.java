package ru.smsfinance.crypto.core.keystore;

import ru.smsfinance.crypto.api.dto.DTOStore;

import java.util.Optional;

/**
 * Store unlocker builder
 */
public final class StoreUnlockerBuilder {

    private StoreUnlockerBuilder() {
    }

    /**
     * Build store unlocker from DTO
     *
     * @param dtoStore dto
     * @return constructed store unlocker
     */
    public static StoreUnlocker buildFrom(DTOStore dtoStore) {
        StoreUnlocker storeUnlocker = new StoreUnlocker();
        Optional<DTOStore> optionalDtoStore = Optional.ofNullable(dtoStore);

        optionalDtoStore
                .map(DTOStore::getAlias)
                .ifPresent(storeUnlocker::setAlias);

        optionalDtoStore
                .map(DTOStore::getKeyPassword)
                .map(String::toCharArray)
                .ifPresent(storeUnlocker::setKeyPassword);

        optionalDtoStore
                .map(DTOStore::getStorePassword)
                .map(String::toCharArray)
                .ifPresent(storeUnlocker::setStorePassword);

        return storeUnlocker;
    }
}
