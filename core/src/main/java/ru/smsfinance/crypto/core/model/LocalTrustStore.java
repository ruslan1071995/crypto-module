package ru.smsfinance.crypto.core.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Trust store entity
 */
@Data
@Entity
@Table(name = "trust_store")
public class LocalTrustStore {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "key_id")
    private String keyId;

    @Column(name = "path")
    private String path;

    @Column(name = "standard")
    private String standard;

    @Column(name = "upload_time")
    private LocalDateTime uploadTime = LocalDateTime.now();
}
