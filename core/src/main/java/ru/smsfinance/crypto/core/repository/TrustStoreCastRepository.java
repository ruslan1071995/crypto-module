package ru.smsfinance.crypto.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.smsfinance.crypto.core.model.LocalTrustStore;
import ru.smsfinance.crypto.core.model.TrustStoreUploadCast;

/**
 * Trust store cast repository
 */
@Repository
public interface TrustStoreCastRepository extends JpaRepository<TrustStoreUploadCast, Long> {

    /**
     * Find last uploaded by trust store
     *
     * @param trustStore local trust store
     * @return local trust store entity
     */
    TrustStoreUploadCast findByTrustStoreOrderByUploadTimeDesc(LocalTrustStore trustStore);
}
