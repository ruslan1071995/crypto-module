package ru.smsfinance.crypto.core.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Local key store entity
 */
@Data
@Entity
@Table(name = "key_store")
@ToString
public class LocalKeyStore {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "key_id")
    private String keyId;

    @Column(name = "upload_time")
    private LocalDateTime uploadTime = LocalDateTime.now();
}
