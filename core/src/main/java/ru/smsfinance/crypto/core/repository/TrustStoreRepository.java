package ru.smsfinance.crypto.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.smsfinance.crypto.core.model.LocalTrustStore;

/**
 * Trust store repository
 */
@Repository
public interface TrustStoreRepository extends JpaRepository<LocalTrustStore, Long> {

    /**
     * Find last uploaded trust store by key id
     *
     * @param keyId key id
     * @return local trust store entity
     */
    LocalTrustStore findFirstByKeyIdOrderByUploadTimeDesc(String keyId);
}
